package com.example.composecodelab

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.Model
import androidx.compose.unaryPlus
import androidx.ui.core.Text
import androidx.ui.core.dp
import androidx.ui.core.setContent
import androidx.ui.graphics.Color
import androidx.ui.layout.Column
import androidx.ui.layout.ExpandedHeight
import androidx.ui.layout.Spacing
import androidx.ui.material.*
import androidx.ui.material.surface.Surface
import androidx.ui.tooling.preview.Preview

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyAppTheme {
                Surface {
                    Greeting("Android")
                }
            }
        }
    }
}

@Composable
fun MyApp(children: @Composable() () -> Unit) {
        Surface{
            Greeting(name = "Android")
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!",
            modifier = Spacing(24.dp),
            style = (+MaterialTheme.typography()).h2
    )
}

@Preview("Text preview")
@Composable
fun DefaultPreview() {
    MyAppTheme {
        Surface {
            Greeting("Android")
        }
    }
}

/*
@Preview("text codelab")
@Composable
fun DefaultPreview() {
    MyApp {
        MyScreenContent()
    }
}
*/

/*@Composable
fun MyScreenContent() {
    Column {
        Greeting("Android")
        Divider(color = Color.Black)
        Greeting("there")
    }
}*/

@Model
class CounterState(var count: Int = 0)

@Composable
fun Counter(state: CounterState) {
    Button(
            text = "I've been clicked ${state.count} times",
            onClick = {
                state.count++
            },
            style = ContainedButtonStyle(color = if (state.count > 5) Color.Green else Color.White)

    )
}


@Composable
fun MyScreenContent(
        names: List<String> = listOf("Android", "there"),
        counterState: CounterState = CounterState()
) {
    Column(modifier = ExpandedHeight) {
        Column(modifier = Flexible(1f)) {
            for (name in names) {
                Greeting(name = name)
                Divider(color = Color.Black)
            }
        }
        Counter(counterState)
    }
}

@Model
class FormState(var optionChecked: Boolean)

@Composable
fun Form(formState: FormState) {
    Checkbox(
            checked = formState.optionChecked,
            onCheckedChange = { newState ->
                formState.optionChecked = newState
                Log.d("newstate", newState.toString())
            })
}
